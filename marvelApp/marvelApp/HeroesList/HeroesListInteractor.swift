//
//  HeroesListInteractor.swift
//  marvelApp
//
//  Created by Ribeiro, P. on 07/04/2018.
//  Copyright © 2018 Ribeiro, P.. All rights reserved.
//

import UIKit

protocol HeroesListBusinessLogic {

    func doSomething(request: HeroesList.Something.Request)
}

protocol HeroesListDataStore {

    //var name: String { get set }
}

class HeroesListInteractor: HeroesListBusinessLogic, HeroesListDataStore {

    var presenter: HeroesListPresentationLogic?
    var worker: HeroesListWorker?
    //var name: String = ""

    // MARK: Do something
    func doSomething(request: HeroesList.Something.Request) {

        worker = HeroesListWorker()
        worker?.doSomeWork()

        let response = HeroesList.Something.Response()
        presenter?.presentSomething(response: response)
    }
}

// MARK: Output --- Present something
extension HeroesListInteractor {

}
