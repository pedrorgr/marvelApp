//
//  HeroesListPresenter.swift
//  marvelApp
//
//  Created by Ribeiro, P. on 07/04/2018.
//  Copyright © 2018 Ribeiro, P.. All rights reserved.
//

import UIKit

protocol HeroesListPresentationLogic {

    func presentSomething(response: HeroesList.Something.Response)
}

class HeroesListPresenter: HeroesListPresentationLogic {

    weak var viewController: HeroesListDisplayLogic?

    // MARK: Do something
    func presentSomething(response: HeroesList.Something.Response) {
        
        let viewModel = HeroesList.Something.ViewModel()
        viewController?.displaySomething(viewModel: viewModel)
    }
}

// MARK: Output --- Display something
extension HeroesListPresenter {

}
