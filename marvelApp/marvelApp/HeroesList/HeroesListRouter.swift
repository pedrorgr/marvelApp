//
//  HeroesListRouter.swift
//  marvelApp
//
//  Created by Ribeiro, P. on 07/04/2018.
//  Copyright © 2018 Ribeiro, P.. All rights reserved.
//

import UIKit

@objc protocol HeroesListRoutingLogic {

    //func routeToSomewhere(segue: UIStoryboardSegue?)
}

protocol HeroesListDataPassing {

    var dataStore: HeroesListDataStore? { get }
}

class HeroesListRouter: HeroesListRoutingLogic, HeroesListDataPassing {

    weak var viewController: HeroesListViewController?
    var dataStore: HeroesListDataStore?

    // MARK: Routing
    
//    func routeToSomewhere(segue: UIStoryboardSegue?) {
//
//        let destinationVC = SomewhereViewController()
//        if let sourceDS = dataStore, var destinationDS = destinationVC.router?.dataStore {
//
//            passData(from: sourceDS, to: &destinationDS)
//        }
//
//        if let sourceVC = viewController {
//            navigate(from: sourceVC, to: destinationVC)
//        }
//    }
//
//    // MARK: Navigation
//
//    func navigate(from source: HeroesListViewController, to destination: SomewhereViewController) {
//
//        source.show(destination, sender: nil)
//    }
//
//    // MARK: Passing data
//
//    func passData(from source: HeroesListDataStore, to destination: inout SomewhereDataStore) {
//
//        destination.name = source.name
//    }
}
