//
//  HeroesListViewController.swift
//  marvelApp
//
//  Created by Ribeiro, P. on 07/04/2018.
//  Copyright © 2018 Ribeiro, P.. All rights reserved.
//

import UIKit

protocol HeroesListDisplayLogic: class {

    func displaySomething(viewModel: HeroesList.Something.ViewModel)
}

class HeroesListViewController: UIViewController {

    var interactor: HeroesListBusinessLogic?
    var router: (HeroesListRoutingLogic & HeroesListDataPassing)?

    private let sceneView = HeroesListView()

    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {

        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)
        setup()
    }

    // MARK: Setup
    private func setup() {

        let viewController = self
        let interactor = HeroesListInteractor()
        let presenter = HeroesListPresenter()
        let router = HeroesListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: View lifecycle
    override func loadView() {
        view = sceneView
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        doSomething()
    }
}

// MARK: Output --- Do something
extension HeroesListViewController {

    func doSomething() {

        let request = HeroesList.Something.Request()
        interactor?.doSomething(request: request)
    }
}

// MARK: Input --- Display something
extension HeroesListViewController: HeroesListDisplayLogic {

    func displaySomething(viewModel: HeroesList.Something.ViewModel) {

        //nameTextField.text = viewModel.name
    }
}

// MARK: Routing --- Navigate next scene
extension HeroesListViewController {

    private func prepareForNextScene() {

        //        router?.routeToNextScene()
    }
}
