//
//  HeroDetailView.swift
//  marvelApp
//
//  Created by Ribeiro, P. on 07/04/2018.
//  Copyright © 2018 Ribeiro, P.. All rights reserved.
//

import UIKit

class HeroDetailView: UIView {

    private struct ViewTraits {

        // Margins
        static let topMargin: CGFloat = 15.0
        static let bottomMargin: CGFloat = 50.0

        //Heights
        static let viewHeight: CGFloat = 150.0
    }

    // MARK: Public
    let simpleView: UIView

    override init(frame: CGRect) {

        //simpleView
        simpleView = UIView()
        simpleView.backgroundColor = .clear

        // Init
        super.init(frame: frame)

        backgroundColor = .white

        // Add subviews
        addSubview(simpleView)

        // Add constraints
        simpleView.translatesAutoresizingMaskIntoConstraints = false

        addCustomConstraints()
    }

    required init?(coder aDecoder: NSCoder) {

        fatalError("init(coder:) has not been implemented")
    }

    private func addCustomConstraints() {

        NSLayoutConstraint.activate([

            // Horizontal
            // Remember to use safeAreaLayoutGuide if iOS 11 and up
            simpleView.leadingAnchor.constraint(equalTo: leadingAnchor),
            simpleView.trailingAnchor.constraint(equalTo: trailingAnchor),

            // Vertical
            simpleView.topAnchor.constraint(equalTo: topAnchor),
            simpleView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
    }
}
