//
//  HeroDetailPresenter.swift
//  marvelApp
//
//  Created by Ribeiro, P. on 07/04/2018.
//  Copyright © 2018 Ribeiro, P.. All rights reserved.
//

import UIKit

protocol HeroDetailPresentationLogic {

    func presentSomething(response: HeroDetail.Something.Response)
}

class HeroDetailPresenter: HeroDetailPresentationLogic {

    weak var viewController: HeroDetailDisplayLogic?

    // MARK: Do something
    func presentSomething(response: HeroDetail.Something.Response) {
        
        let viewModel = HeroDetail.Something.ViewModel()
        viewController?.displaySomething(viewModel: viewModel)
    }
}

// MARK: Output --- Display something
extension HeroDetailPresenter {

}
