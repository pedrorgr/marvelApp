//
//  HeroDetailInteractor.swift
//  marvelApp
//
//  Created by Ribeiro, P. on 07/04/2018.
//  Copyright © 2018 Ribeiro, P.. All rights reserved.
//

import UIKit

protocol HeroDetailBusinessLogic {

    func doSomething(request: HeroDetail.Something.Request)
}

protocol HeroDetailDataStore {

    //var name: String { get set }
}

class HeroDetailInteractor: HeroDetailBusinessLogic, HeroDetailDataStore {

    var presenter: HeroDetailPresentationLogic?
    var worker: HeroDetailWorker?
    //var name: String = ""

    // MARK: Do something
    func doSomething(request: HeroDetail.Something.Request) {

        worker = HeroDetailWorker()
        worker?.doSomeWork()

        let response = HeroDetail.Something.Response()
        presenter?.presentSomething(response: response)
    }
}

// MARK: Output --- Present something
extension HeroDetailInteractor {

}
