//
//  HeroDetailViewController.swift
//  marvelApp
//
//  Created by Ribeiro, P. on 07/04/2018.
//  Copyright © 2018 Ribeiro, P.. All rights reserved.
//

import UIKit

protocol HeroDetailDisplayLogic: class {

    func displaySomething(viewModel: HeroDetail.Something.ViewModel)
}

class HeroDetailViewController: UIViewController {

    var interactor: HeroDetailBusinessLogic?
    var router: (HeroDetailRoutingLogic & HeroDetailDataPassing)?

    private let sceneView = HeroDetailView()

    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {

        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)
        setup()
    }

    // MARK: Setup
    private func setup() {

        let viewController = self
        let interactor = HeroDetailInteractor()
        let presenter = HeroDetailPresenter()
        let router = HeroDetailRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: View lifecycle
    override func loadView() {
        view = sceneView
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        doSomething()
    }
}

// MARK: Output --- Do something
extension HeroDetailViewController {

    func doSomething() {

        let request = HeroDetail.Something.Request()
        interactor?.doSomething(request: request)
    }
}

// MARK: Input --- Display something
extension HeroDetailViewController: HeroDetailDisplayLogic {

    func displaySomething(viewModel: HeroDetail.Something.ViewModel) {

        //nameTextField.text = viewModel.name
    }
}

// MARK: Routing --- Navigate next scene
extension HeroDetailViewController {

    private func prepareForNextScene() {

        //        router?.routeToNextScene()
    }
}
